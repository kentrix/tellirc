FROM rust:1.34

WORKDIR /bot
COPY . .

RUN cargo install --path .
CMD ["/usr/local/cargo/bin/tellirc"]
