# TellIRC Bot
Save your messages and tell the person you are after!

# Example config file
server = "irc.freenode.net"
port = 6667
channels = ["example1", "example2"]
nickname = "tellbot"
