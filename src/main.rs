extern crate irc;
extern crate rusqlite;
extern crate tellirc;

use irc::client::prelude::*;
use rusqlite::Connection;
use tellirc::{new_config_from_file, make_handler_fn};

fn main() {
    let mut args = std::env::args();
    // Skip $0
    args.next();
    let file_name = match args.next() {
        Some(file) => file,
        None => { eprintln!("You must specify a config file"); return; }
    };
    let config = new_config_from_file(&file_name).unwrap();
    let sqlite_database_file = args.next().unwrap_or_else(|| "tellirc.sqlite".to_owned());

    let mut reactor = IrcReactor::new().unwrap();
    loop {
        let client = reactor.prepare_client_and_connect(&config).unwrap();
        client.identify().unwrap();
        let sql_conn = Connection::open(sqlite_database_file.clone()).unwrap();
        let dao = tellirc::Dao::new(sql_conn);

        reactor.register_client_with_handler(client, make_handler_fn(dao));

        let run_res = reactor.run();
        match run_res {
            Ok(_) => break,
            Err(e) => { eprintln!("Error encountered: {:?}", e); eprintln!("Restarting...") },
        }
    }
}
