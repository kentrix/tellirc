DROP TABLE MessageCache;

create table MessageCache
(
    id INTEGER not null
    constraint MessageCache_pk
    primary key autoincrement,
    to_name text not null,
    message text not null,
    in_chan text,
    from_name text not null,
    iso_date text not null
);
