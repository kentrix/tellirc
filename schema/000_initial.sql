
create table Aliases
(
    id INTEGER not null
    constraint Aliases_pk
    primary key autoincrement
    unique,
    aliases BLOB
);

create table MessageCache
(
    id INTEGER not null
    constraint MessageCache_pk
    primary key autoincrement,
    to_name text not null,
    message text not null,
    in_chan text,
    from_name text not null
);

create unique index MessageCache_id_uindex
on MessageCache (id);

